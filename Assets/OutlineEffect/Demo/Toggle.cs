﻿using UnityEngine;

namespace cakeslice
{
    public class Toggle : MonoBehaviour
    {
        void Start()
        {
            GetComponentInChildren<Outline>().enabled = false;
        }

        void TurnOn()
        {
            GetComponentInChildren<Outline>().enabled = true;
        }
        void TurnOff()
        {
            if(transform.tag != "Skeleton")
                GetComponentInChildren<Outline>().enabled = false;
        }
    }
}