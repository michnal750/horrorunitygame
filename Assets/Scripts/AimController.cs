﻿using UnityEngine;

public class AimController : MonoBehaviour
{
	public GameObject aimingPoint;
	public Camera camera;
	public float range = 2f;
	private Transform pickedUpObject;
	private Transform lookedAtObject;
	private Inventory inventory;

	private bool holding = false;

	void Start()
	{
		inventory = GetComponent<Inventory>();
	}


	[System.Obsolete]
	void Update()
	{
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		Debug.DrawRay(ray.origin, ray.direction);
		RaycastHit hit;

		if (!holding)
		{
			if (Physics.Raycast(ray, out hit, range))
			{
				if (hit.transform.tag == "Gate" || hit.transform.tag == "Disc" || hit.transform.tag == "Pickable Object" || hit.transform.tag == "Collectable Object" || hit.transform.tag == "Note Object" || hit.transform.tag == "Door" || hit.transform.tag == "Hammer" || hit.transform.tag == "Safe" || hit.transform.tag == "Button" || hit.transform.tag == "Screen" || hit.transform.tag == "Skeleton")
				{
					if (lookedAtObject == null)
					{
						lookedAtObject = hit.transform;
					}
					else
					{
						lookedAtObject = hit.transform;
						if (lookedAtObject.tag == "Skeleton")
							Debug.Log("Skeleton");
						lookedAtObject.BroadcastMessage("TurnOn");
					}
					if (Input.GetKeyDown(KeyCode.E))
					{
						if (hit.transform.tag == "Pickable Object" || hit.transform.tag == "Hammer")
						{
							pickedUpObject = lookedAtObject;
							pickedUpObject.SendMessage("Take");
							holding = true;
						}
						else if (hit.transform.tag == "Collectable Object" || hit.transform.tag == "Note Object")
						{
							hit.transform.SendMessage("Collect");
						}
						else if (hit.transform.tag == "Door")
						{
							hit.transform.SendMessage("ChangeState", inventory.items);
						}
						else if (lookedAtObject.tag == "Safe")
						{
							lookedAtObject.BroadcastMessage("OpenOrClose");
						}
						else if (lookedAtObject.tag == "Button")
						{
							lookedAtObject.BroadcastMessage("PressButton");
						}
						else if (lookedAtObject.tag == "Screen")
						{
							lookedAtObject.BroadcastMessage("TurnOnScreen");
						}
						else if (hit.transform.tag == "Skeleton")
						{
							hit.transform.SendMessage("SwapItems");
						}
						else if(hit.transform.tag == "Disc")
						{
							lookedAtObject.SendMessage("Rotate");
						}
						else if(hit.transform.tag == "Gate")
						{
							lookedAtObject.SendMessage("Open");
						}
					}
					else if (lookedAtObject.tag == "Safe")
					{
						int number = CheckNumberPressed();
						if (number != -1)
						{
							lookedAtObject.BroadcastMessage("ChangeState", number);
						}
					}
					
				}
				else
				{
					if (lookedAtObject != null && lookedAtObject.tag != "Skeleton")
					{
						lookedAtObject.BroadcastMessage("TurnOff");
						lookedAtObject = null;
					}
				}
			}
			else if (lookedAtObject != null && lookedAtObject.tag != "Skeleton")
			{
				lookedAtObject.BroadcastMessage("TurnOff");
				lookedAtObject = null;
			}
		}
		else
		{
			pickedUpObject.SendMessage("TurnOff");
			lookedAtObject = null;
			if (Input.GetKeyDown(KeyCode.E))
			{
				lookedAtObject = null;
				pickedUpObject.SendMessage("Drop");
				pickedUpObject = null;
				holding = false;
			}
			else if (Input.GetKeyDown(KeyCode.Mouse0))
			{
				if (pickedUpObject.tag == "Hammer")
				{
					pickedUpObject.SendMessage("Swing");
				}
				else
				{
					lookedAtObject = null;
					pickedUpObject.SendMessage("Throw");
					pickedUpObject = null;
					holding = false;
				}
			}
		}
	}

	int CheckNumberPressed()
	{
		KeyCode[] keyCodes = {
		 KeyCode.Alpha0,
		 KeyCode.Alpha1,
		 KeyCode.Alpha2,
		 KeyCode.Alpha3,
		 KeyCode.Alpha4,
		 KeyCode.Alpha5,
		 KeyCode.Alpha6,
		 KeyCode.Alpha7,
		 KeyCode.Alpha8,
		 KeyCode.Alpha9,
		 KeyCode.R
	 };
		int numberPressed = -1;

		for (int i = 0; i < keyCodes.Length; i++)
		{
			if (Input.GetKeyDown(keyCodes[i]))
			{
				numberPressed = i;
				Debug.Log(numberPressed);

			}
		}
		return numberPressed;
	}
}
