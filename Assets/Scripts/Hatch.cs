﻿using UnityEngine;

public class Hatch : MonoBehaviour
{
    private Rigidbody hatchRigidbody;
    void Start()
    {
        hatchRigidbody = GetComponent<Rigidbody>();
    }

    public void Open()
    {
        hatchRigidbody.isKinematic = false;
    }
}
