﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dynamite : MonoBehaviour
{
    public float delay = 3f;
    public float force = 800f;
    public float radius = 4f;
    bool ticking = false;
    float countdown;

    public GameObject explosion;
    public GameObject dustExplosion;
    
    void Update()
    {
        if(ticking)
        {
            countdown -= Time.deltaTime;
            if(countdown<=0)
            {
                Explode();
            }
        }
    }

    void Explode()
    {
        GameObject ex = Instantiate(explosion, transform.position, transform.rotation);
        ex.transform.SetParent(transform.parent);
        GameObject du = Instantiate(dustExplosion, transform.position, transform.rotation);
        du.transform.SetParent(transform.parent);
        Destroy(gameObject);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(force, transform.position, radius);
            }
        }
    }

    void BlastOff()
    {
        countdown = delay;
        ticking = true;
    }
}
