﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour
{
	public float force = 5f;
	private void Open()
	{
		GetComponent<Rigidbody>().AddForce(new Vector3(force, 0, 0), ForceMode.Impulse);
	}

	private void Close()
	{
		GetComponent<Rigidbody>().AddForce(new Vector3(force, 0, 0), ForceMode.Impulse);
	}
}
