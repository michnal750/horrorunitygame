﻿using UnityEngine;

public class NoteUI : MonoBehaviour
{
    private Inventory inventory;
    public NoteSlot[] noteSlots;

    void Awake()
    {
        noteSlots = GetComponentsInChildren<NoteSlot>();
        inventory = Inventory.instance;
        inventory.onNoteChangedCallback += UpdateUI;
    }

    void UpdateUI()
    {
        Debug.Log("updated notes list");
        for (int i = 0; i < noteSlots.Length; i++)
        {
            if (i < inventory.notes.Count)
            {
                noteSlots[i].AddItem(inventory.notes[i]);
            }
            else
            {
               // inventorySlots[i].ClearSlot();
            }
        }
    }
}
