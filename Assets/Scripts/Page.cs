﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Page", menuName = "Inventory/Page")]
public class Page : ScriptableObject
{
    public string label;
    public int number;
    [TextArea(16, 8)]
    public string text;
}

