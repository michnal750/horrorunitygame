﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPress : MonoBehaviour
{
    public GameObject gate;

    [SerializeField]
    string actionFunctionName;
    float pressDistance = 0.025f;
    bool ticking = false;
    float countdown;
    float delay = 1f;

    private void Start()
    {
        if (actionFunctionName == "")
        {
            actionFunctionName = "Open";
        }
    }
    private void Update()
    {
        if (ticking)
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0)
            {
                Move();
                ticking = false;
            }
        }
    }

    void Move()
    {
        pressDistance *= -1;
        transform.Translate(0, 0, -pressDistance);
        ticking = true;
    }

    void PressButton()
    {
        FindObjectOfType<AudioManager>().Play("FlashlightOn");
        if (!ticking)
        {
            countdown = delay;
            Move();
            gate.SendMessage(actionFunctionName);
            ticking = true;
        }
    }
}
