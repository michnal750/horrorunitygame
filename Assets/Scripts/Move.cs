﻿using UnityEngine;

public class Move : MonoBehaviour
{
    public CharacterController characterController;
    public GameObject camera;
    private Vector3 aimingPointPosition;
    public float range = 0.5f;

    // Update is called once per frame
    void Update()
    {
        transform.position = Camera.main.transform.position + Camera.main.transform.forward * range;
    }
}
