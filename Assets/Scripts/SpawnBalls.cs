﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBalls : MonoBehaviour
{
    public GameObject spawner;
    public GameObject goldenBallClone;
    public GameObject balls;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent.tag == "Balls")
        {
            Destroy(other);
            GameObject obj = Instantiate(goldenBallClone, spawner.transform.position, spawner.transform.rotation, balls.transform);
        }
    }


}
