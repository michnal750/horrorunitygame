﻿using UnityEngine;

public class MonsterZone : MonoBehaviour
{
    public int zoneNumber;
    private MonsterManager monsterManager;

    private void Start()
    {
        monsterManager = FindObjectOfType<MonsterManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            monsterManager.StartFollowPlayer(zoneNumber);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            monsterManager.StopFollowPlayer(zoneNumber);
        }
    }

}
