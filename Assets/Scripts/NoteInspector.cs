﻿using TMPro;
using UnityEngine;

public class NoteInspector : MonoBehaviour
{
    private Note note;
    public TextMeshProUGUI noteText;
    public TextMeshProUGUI numberOfPageText;
    private int actualPageNumber = 0;

    public GameObject selector;
    public GameObject notePanel;

    public void SetNoteInspector(Note newNote)
    {
        selector.SetActive(false);
        notePanel.SetActive(false);
        actualPageNumber = 0;
        note = newNote;
        noteText.text = note.pages[actualPageNumber].text;
        numberOfPageText.text = "Page " + (actualPageNumber + 1) + " / " + note.pages.Length;
    }

    public void OnCloseButtonClick()
    {
        selector.SetActive(true);
        notePanel.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnNextPageButtonClick()
    {
        if (actualPageNumber + 1 < note.pages.Length)
        {
            FindObjectOfType<AudioManager>().Play("pageFlip");
            ++actualPageNumber;
            noteText.text = note.pages[actualPageNumber].text;
            numberOfPageText.text = "Page " + (actualPageNumber + 1) + " / " + note.pages.Length;
        }
    }

    public void OnPreviousPageButtonClick()
    {
        if (actualPageNumber > 0)
        {
            FindObjectOfType<AudioManager>().Play("pageFlip");
            --actualPageNumber;
            noteText.text = note.pages[actualPageNumber].text;
            numberOfPageText.text = "Page " + (actualPageNumber + 1) + " / " + note.pages.Length;
        }
    }
}
