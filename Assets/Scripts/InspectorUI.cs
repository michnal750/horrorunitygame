﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InspectorUI : MonoBehaviour
{
	private Item item;
	public Image image;
	public TextMeshProUGUI text;
	private bool isEmpty = true;

	public GameObject inventoryPanel;
	public GameObject room;
	public Camera mainCamera;
	public GameObject mainLight;
	public Transform playerTransform;

	public void LoadItemToInspector(Item newItem)
	{
		item = newItem;
		image.sprite = item.icon;
		image.enabled = true;
		text.text = item.name;
		isEmpty = false;
	}

	public void ClearInspector()
	{
		item = null;
		image.enabled = false;
		text.text = "";
		isEmpty = true;
	}

	public void OnRemoveButton()
	{
		if (!isEmpty)
		{
			Debug.Log("Wyrzucam przedmiot");
			GameObject obj = Instantiate(item.gameObjectClone, playerTransform.position, playerTransform.rotation);
			obj.transform.SetParent(null);
			Inventory.instance.Remove(item);
			ClearInspector();
		}
	}

	public void OnExamineButton()
	{
		if (!isEmpty)
		{
			Debug.Log("Badam przedmiot");
			inventoryPanel.SetActive(false);
			room.SetActive(true);
			room.SendMessage("SetItem", item);
			room.SendMessage("SetDescription", item.description);
			room.SendMessage("Spawn");
			mainCamera.enabled = false;
			mainLight.SetActive(false);
			Time.timeScale = 1f;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}

	public void OnUseButon()
	{
		if (!isEmpty)
		{
			Debug.Log("Uzywam przedmiotu");
			item.Use();
			ClearInspector();
		}
	}
}
