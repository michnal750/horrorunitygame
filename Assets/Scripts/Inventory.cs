﻿using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        instance = this;
    }
    #endregion

    //items
    public List<Item> items = new List<Item>();
    public int capacity = 20;
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    //notes
    public List<Note> notes = new List<Note>();
    public delegate void OnNoteChanged();
    public OnItemChanged onNoteChangedCallback;

    public bool Add(Item item)
    {
        if (items.Count < capacity)
        {
            Debug.Log("Add item to inventory");
            items.Add(item);
            if (item.name == "Flashlight")
            {
                FindObjectOfType<Flashlight>().EquipWithFlashlight();
            }
            if (onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
            return true;
        }
        else
        {
            Debug.Log("Not enough slots in inventory");
            return false;
        }
    }
    public void Remove(Item item)
    {
        items.Remove(item);
        if (item.name == "Flashlight")
        {
            FindObjectOfType<Flashlight>().RemoveFlashlight();
        }
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }

    public bool Add(Note note)
    {
        Debug.Log("Add note to inventory");
        notes.Add(note);
        if (onNoteChangedCallback != null)
        {
            onNoteChangedCallback.Invoke();
        }
        return true;
    }
    public void Remove(Note note)
    {
        notes.Remove(note);

        if (onNoteChangedCallback != null)
        {
            onNoteChangedCallback.Invoke();
        }
    }
}
