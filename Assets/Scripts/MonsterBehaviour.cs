﻿using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterBehaviour : MonoBehaviour
{
    private Animator animator;

    // evade terrain obstacle
    private bool isEvadingObstacle;
    public Transform obstaclesCheck;
    public float checkDistance = 0.5f;
    public Vector3 rotation;
    public LayerMask obstaclesMask;
    private float beginningRotationY;
    public float maxRotation;

    // walking to destination point
    public Transform destinationPoint;
    public float walkSpeed = 1.5f;
    private bool isWalking;
    private bool isIdle;

    // chasing player
    public Transform player;
    public float runSpeed = 10.0f;
    public float sightRange = 10.0f;
    public float sightAngle = 60.0f;
    private bool isChasing;
    private float time = 0.0f;

    //zone
    private bool playerInZone;
    public Transform[] startPoint;
    private int actualZoneNumber;

    void Start()
    {
        animator = GetComponent<Animator>();
        isWalking = true;
        animator.SetBool("isWalking", isWalking);
    }

    void Update()
    {
        if (isIdle)
        {
            if (Vector3.Magnitude(transform.position - destinationPoint.position) > 2.0f)
            {
                isIdle = false;
                isWalking = true;
                animator.SetBool("isWalking", isWalking);
            }
        }
        else if (isEvadingObstacle)
        {
            EvadeObstacle();
        }
        else if (isWalking)
        {
            DetectObstacle();
            Walk();
        }
        else if (isChasing)
        {
            DetectObstacle();
            Chase();
            if (DetectPlayer())
            {
                time = 0.0f;
            }
            else
            {
                time += Time.deltaTime;
            }
            if (time > 5.0f)
            {
                Debug.Log("Przestaje gonic gracza");
                isChasing = false;
                isWalking = true;
                animator.SetBool("isRunning", isChasing);
                animator.SetBool("isWalking", isWalking);
            }
        }
        if (playerInZone && !isChasing && DetectPlayer())
        {
            time = 0.0f;
            Debug.Log("Zaczynam gonic gracza");
            isChasing = true;
            isWalking = false;
            animator.SetBool("isRunning", isChasing);
            animator.SetBool("isWalking", isWalking);
        }
    }

    public void PlayerEnterZone()
    {
        playerInZone = true;
    }

    public void PlayerExitZone()
    {
        playerInZone = false;
        if (isChasing)
        {
            Debug.Log("Przestaje gonic gracza");
            isChasing = false;
            isWalking = true;
            animator.SetBool("isRunning", isChasing);
            animator.SetBool("isWalking", isWalking);
        }
        destinationPoint.position = startPoint[actualZoneNumber].position;
    }

    private void Chase()
    {
        float rotateStep = 1.5f * Time.deltaTime;
        Vector3 targetDirection = player.position - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, rotateStep, 0.0f);
        transform.rotation = Quaternion.Euler(0, Quaternion.LookRotation(newDirection).eulerAngles.y, 0);
        transform.Translate(0, 0, runSpeed * Time.deltaTime);
        if (Vector3.Magnitude(transform.position - player.position) < 3.0f)
        {
            Death();
        }
    }

    private void Walk()
    {
        float rotateStep = 0.25f * Time.deltaTime;
        Vector3 targetDirection = destinationPoint.position - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, rotateStep, 0.0f);
        transform.rotation = Quaternion.Euler(0, Quaternion.LookRotation(newDirection).eulerAngles.y, 0);
        transform.Translate(0, 0, walkSpeed * Time.deltaTime);

        if (Vector3.Magnitude(transform.position - destinationPoint.position) < 2.0f)
        {
            if (startPoint[actualZoneNumber].position == destinationPoint.position)
            {
                isWalking = false;
                isIdle = true;
                animator.SetBool("isWalking", isWalking);
            }
            else
            {
                destinationPoint.position = startPoint[actualZoneNumber].position;
            }
        }

    }

    private void DetectObstacle()
    {
        Vector3 pos1 = transform.position;
        pos1.y += 1.2f;
        if (!isEvadingObstacle && Physics.CapsuleCast(pos1, obstaclesCheck.position, 0.4f, transform.forward, checkDistance, obstaclesMask))
        {
            Debug.Log("Przeszkoda !!!");
            isEvadingObstacle = true;
            beginningRotationY = transform.rotation.y;
            if (Random.Range(0, 2) == 0)
            {
                rotation *= -1;
            }
        }
    }

    private void EvadeObstacle()
    {
        if (isWalking)
        {
            transform.Rotate(rotation * Time.deltaTime);
            transform.Translate(0, 0, walkSpeed * Time.deltaTime);
            Debug.Log(Mathf.Abs(transform.rotation.y - beginningRotationY));
            if (Mathf.Abs(transform.rotation.y - beginningRotationY) >= maxRotation)
            {
                isEvadingObstacle = false;
            }
        }
        else if (isChasing)
        {
            transform.Rotate(3.0f * rotation * Time.deltaTime);
            transform.Translate(0, 0, runSpeed * Time.deltaTime);
            Debug.Log(Mathf.Abs(transform.rotation.y - beginningRotationY));
            if (Mathf.Abs(transform.rotation.y - beginningRotationY) >= maxRotation)
            {
                isEvadingObstacle = false;
            }
        }
    }

    private bool DetectPlayer()
    {
        if (Vector3.Magnitude(transform.position - player.position) < sightRange)
        {
            Vector3 targetDirection = player.position - transform.position;
            float angle = Vector3.Angle(targetDirection, transform.forward);
            if (angle <= sightAngle)
            {
                return true;
            }
        }
        return false;
    }

    private void Death()
    {
        FindObjectOfType<PlayerMovement>().enabled = false;
        animator.SetBool("isAttacking", true);
        FindObjectOfType<GameManager>().EndGame();
    }

    public void NewZone(int zoneNumber)
    {
        actualZoneNumber = zoneNumber;
        transform.position = startPoint[zoneNumber].position;
    }
}
