﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
	public GameObject destructibleVersion;		

	public void Substitute()
	{
		Instantiate(destructibleVersion, transform.position, transform.rotation);
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "HitPoint")
		{
			Instantiate(destructibleVersion, transform.position, transform.rotation);
			Destroy(gameObject);
		}
		
	}
}
