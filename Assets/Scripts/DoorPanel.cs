﻿using UnityEngine;


public class DoorPanel : MonoBehaviour
{

    bool locked = true;
    public string userInput;
    private int length;
    public string code;
    public GameObject[] hashes = new GameObject[4];
    public GameObject interactiveObject;
    public Vector3 offset;

    void Start()
    {
        foreach (GameObject go in hashes)
        {
            go.SetActive(false);
        }
        userInput = "";
    }

    void Update()
    {
        length = userInput.Length;
        for (int i = 0; i < length; i++)
            hashes[i].SetActive(true);
        int remove = 4 - length;
        for (int i = 3; i > 3 - remove; i--)
            hashes[i].SetActive(false);
    }

    void OpenOrClose()
    {
        CompareCodes();
    }
    private enum LockState
    {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        rKey //= KeyCode.R

    }
    void ChangeState(int input)
    {

        if (!locked)
            return;

        LockState var = (LockState)input;
        switch (var)
        {
            case LockState.Zero:
                userInput += '0';

                break;
            case LockState.One:
                userInput += '1';

                break;
            case LockState.Two:
                userInput += '2';

                break;
            case LockState.Three:
                userInput += '3';

                break;
            case LockState.Four:
                userInput += '4';

                break;
            case LockState.Five:
                userInput += '5';

                break;
            case LockState.Six:
                userInput += '6';

                break;
            case LockState.Seven:
                userInput += '7';

                break;
            case LockState.Eight:
                userInput += '8';

                break;
            case LockState.Nine:
                userInput += '9';

                break;
            case (LockState)10:
                if(userInput.Length > 0)
                {
                    userInput = userInput.Remove(userInput.Length - 1, 1);
                }
                break;
            default:
                break;
        }
        length = userInput.Length;
        if (length > 4)
            userInput = userInput.Remove(userInput.Length - 1, 1);

    }

    void CompareCodes()
    {
        if (userInput == code && locked)
        {
            locked = false;
            interactiveObject.SendMessage("Open");
            return;

        }
    }


}
