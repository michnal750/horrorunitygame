﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public GameObject effects;
    public float restartDelay = 1f;
    public float vignetteIntensityChange = 0.003f;
    public GameObject deathScreen;

    bool gameEnded = false;
    bool dead = false;
    float timer;

    Vignette vignette = null;
    void Update()
    {
        if (dead)
        {
            deathScreen.SetActive(true);
            deathScreen.GetComponent<CanvasGroup>().alpha += vignetteIntensityChange;
            effects.GetComponent<PostProcessVolume>().profile.TryGetSettings(out vignette);
            vignette.intensity.value += vignetteIntensityChange;
            timer += 0.01f;
            if (timer >= restartDelay)
            {
                Restart();
            }
        }
    }

    public void EndGame()
    {
        if (!gameEnded)
        {
            dead = true;
            gameEnded = true;
        }
    }

    void Restart()
    {
        FindObjectOfType<LoadManager>().TakeOrder();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
