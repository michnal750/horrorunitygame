﻿using System;
using TMPro;
using UnityEngine;

public class RoomControler : MonoBehaviour
{
    public PauseManager pauseManager;
    public Camera examinedCamera;
    public Camera mainCamera;
    private Item item;
    public GameObject mainLight;
    private GameObject examinedObject;
    public Transform center;
    public TextMeshProUGUI description;
    public void SetItem(Item item) => this.item = item;
    public void SetDescription(String text) => description.text = text;

    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            gameObject.SetActive(false);
            Destroy(examinedObject);
            mainLight.SetActive(true);
            mainCamera.enabled = true;
            pauseManager.Pause();
        }
    }

    public void Spawn()
    {
        examinedObject = Instantiate(item.gameObjectClone, center.position, center.rotation);
        examinedObject.GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
        var type = Type.GetType("Watch");
        examinedObject.AddComponent(type);
        examinedObject.GetComponent<Watch>().camera = examinedCamera;
        examinedObject.GetComponent<Watch>().mouseSensitivity = 100;
        examinedObject.GetComponent<Watch>().minFov = 30;
        examinedObject.GetComponent<Watch>().maxFov = 60;
        examinedObject.GetComponent<Watch>().mouseScrollSensitivity = 10;
        examinedObject.transform.localScale *= item.examinedScale;
    }
}
