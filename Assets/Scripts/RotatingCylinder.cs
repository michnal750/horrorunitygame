﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCylinder : MonoBehaviour
{
    private Vector3 targetedRotation;
    private Vector3 currentRotation;
    public float rotateSpeed;
    private bool rotating = false;
    void Start()
    {
        currentRotation = transform.eulerAngles;
    }
    private void Rotate()
    {
        if (!rotating)
        {
            targetedRotation = new Vector3(currentRotation.x, currentRotation.y + 15f, currentRotation.z);
            rotating = true;
        }
    }

    void Update()
    {
        if(rotating)
        { 
        currentRotation = new Vector3(
            Mathf.Lerp(currentRotation.x, targetedRotation.x, Time.deltaTime * rotateSpeed),
            Mathf.Lerp(currentRotation.y, targetedRotation.y, Time.deltaTime * rotateSpeed),
            Mathf.Lerp(currentRotation.z, targetedRotation.z, Time.deltaTime * rotateSpeed));

            transform.eulerAngles = currentRotation;
            if (Mathf.Abs(currentRotation.y - targetedRotation.y) <= 1)
            {
                transform.eulerAngles = targetedRotation;
                rotating = false;
                currentRotation = targetedRotation;
            }
        }
    }

}
