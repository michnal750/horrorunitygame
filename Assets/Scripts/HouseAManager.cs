﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseAManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gate;
    private bool isClosed;
    // Start is called before the first frame update
    void Start()
    {
        isClosed = true;
        gate.SetActive(isClosed);
        
    }
    
    void Toggle()
    {
        isClosed = !isClosed;
        gate.SetActive(isClosed);
    }
}
