﻿using UnityEngine;

public class Button : MonoBehaviour
{
    private bool isPressed;
    public Vector3 offset;
    public GameObject interactiveObject;

    private void PressButton()
    {
        if (!isPressed)
        {
            interactiveObject.SendMessage("Open");
            transform.Translate(-offset);
            isPressed = true;
            FindObjectOfType<AudioManager>().Play("FlashlightOn");
        }
        else
        {
            interactiveObject.SendMessage("Close");
            transform.Translate(offset);
            isPressed = false;
            FindObjectOfType<AudioManager>().Play("FlashlightOff");
        }
    }
}
