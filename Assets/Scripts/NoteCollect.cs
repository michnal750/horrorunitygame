﻿using UnityEngine;

public class NoteCollect : MonoBehaviour
{
    public Note note;
    void Collect()
    {
        Debug.Log("Collected " + note.name);
        bool wasCollected = Inventory.instance.Add(note);
        if (wasCollected)
        {
            Destroy(gameObject);
        }
        else
        {

        }
    }
}
