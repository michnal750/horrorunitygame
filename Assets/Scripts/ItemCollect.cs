﻿using UnityEngine;

public class ItemCollect : MonoBehaviour
{
    public Item item;
    void Collect()
    {
        Debug.Log("Collected " + item.name);
        bool wasCollected = Inventory.instance.Add(item);
        if (wasCollected)
        {
            Destroy(gameObject);
        }
        else
        {

        }
    }
}
