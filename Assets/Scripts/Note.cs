﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Note", menuName = "Inventory/Note")]
public class Note : ScriptableObject
{
    new public string name;
    public Sprite icon;
    public Page[] pages;

    public virtual void Use()
    {
        Debug.Log("Using " + name);
    }
}
