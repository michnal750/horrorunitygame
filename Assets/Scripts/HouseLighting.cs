﻿using UnityEngine;

public class HouseLighting : MonoBehaviour
{
    public Light[] lights;
    public float lightingIntensivity = 3.0f;

    private void Start() => Close();

    public void Open()
    {
        foreach (Light light in lights)
        {
            light.intensity = lightingIntensivity;
        }
    }

    public void Close()
    {
        foreach (Light light in lights)
        {
            light.intensity = 0.0f;
        }
    }
}
