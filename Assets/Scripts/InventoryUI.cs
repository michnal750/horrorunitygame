﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    private Inventory inventory;
    public InventorySlot[] inventorySlots;

    void Awake()
    {
        inventorySlots = GetComponentsInChildren<InventorySlot>();
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
    }

    void UpdateUI()
    {
        Debug.Log("updated inventory");
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                inventorySlots[i].AddItem(inventory.items[i]);
            }
            else
            {
                inventorySlots[i].ClearSlot();
            }
        }
    }
}
