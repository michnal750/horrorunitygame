﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float amount = 50f;

    void Rotate()
    {
        float h = Input.GetAxis("Horizontal") * amount * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * amount * Time.deltaTime;

        GetComponent<Rigidbody>().AddTorque(transform.up * h);
        GetComponent<Rigidbody>().AddTorque(transform.right * v);
    }
}
