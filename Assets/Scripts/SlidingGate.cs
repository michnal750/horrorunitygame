﻿using System;
using UnityEngine;

public class SlidingGate : MonoBehaviour
{
    private bool isOpening, isClosing;
    private Vector3 startPosition;
    private Vector3 dir;
    public bool isUsingZAxis = false;
    public float moveValue = 5.0f;
    public float moveSpeed = -0.5f;

    private void Start()
    {
        if (isUsingZAxis)
        {
            dir = new Vector3(0, 0, moveSpeed);
        }
        else
        {
            dir = new Vector3(moveSpeed, 0, 0);
        }
        startPosition = transform.position;
    }

    public void Open()
    {
        if (!isOpening)
        {
            isOpening = true;
            isClosing = false;
        }
    }

    public void Close()
    {
        if (!isClosing)
        {
            isClosing = true;
            isOpening = false;
        }
    }

    private void Update()
    {
        if (isClosing)
        {
            gameObject.transform.Translate(-dir * Time.deltaTime);
            if (Math.Abs(transform.position.x - startPosition.x) < 0.1f
                || Math.Abs(transform.position.z - startPosition.z) < 0.1f)
            {
                isClosing = false;
            }
        }
        else if (isOpening)
        {
            gameObject.transform.Translate(dir * Time.deltaTime);
            if (Math.Abs(transform.position.x - startPosition.x) > moveValue
                || Math.Abs(transform.position.z - startPosition.z) > moveValue)
            {
                isOpening = false;
            }
        }
    }
}
