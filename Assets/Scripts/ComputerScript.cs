﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerScript : MonoBehaviour
{
    public DoorMechanism[] doors = new DoorMechanism[2];
    public LabButton[] buttons = new LabButton[6];
    public GameObject[] Screenes = new GameObject[2];
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (areTrueButtonsPressed() && !areFalseButtonsPressed())
        {

            doors[0].locked = false;
            doors[1].locked = false;
        }
        if (areFalseButtonsPressed())
        {
            doors[0].locked = true;
            doors[1].locked = true;
        }
       
    }



    bool areFalseButtonsPressed()
    {
       if (buttons[3].pressed || buttons[4].pressed || buttons[5].pressed)
            return true;
       else
            return false;
    }

    bool areTrueButtonsPressed()
    {
        if (buttons[0].pressed && buttons[1].pressed && buttons[2].pressed)
            return true;
        else
            return false;
    }

    void TurnOnScreen()
    {
        if (areTrueButtonsPressed() && !areFalseButtonsPressed())
        {   
            Screenes[0].SetActive(false);
        Screenes[1].SetActive(true); 
        }
    }
}
