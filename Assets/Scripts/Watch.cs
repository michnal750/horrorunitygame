﻿using UnityEngine;

public class Watch : MonoBehaviour
{
    public Camera camera;
    public float mouseSensitivity;
    public float minFov;
    public float maxFov;
    public float mouseScrollSensitivity;

    public GameObject examinedObject;
    private float xRotation = 0.0f;
    private float yRotation = 0.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {               
        GetComponent<Transform>().Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * mouseSensitivity);

        float fov = camera.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * mouseScrollSensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        camera.fieldOfView = fov;
    }
}
