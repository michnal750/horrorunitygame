﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeLock : MonoBehaviour
{
    public bool locked = true;
    public bool opened = false;
    public string code;  
    public float rotateSpeed;  
    public GameObject rotatingDisc;
    public GameObject digitsDisc;

    private Vector3 currentRotationOfRotatingDisc;
    private Vector3 targetedRotationOfRotatingDisc;

    private Vector3 currentRotationOfDigitsDisc;
    private Vector3 targetedRotationOfDigitsDisc;

    private float lastRotation;
    public string userInput;
    private float turn;
    private bool rotating = false;
    private float zeroX;
    void Start()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        turn = 1f;
        lastRotation = 0f;
        currentRotationOfRotatingDisc = rotatingDisc.transform.eulerAngles;
        currentRotationOfDigitsDisc = digitsDisc.transform.eulerAngles;
        zeroX = currentRotationOfRotatingDisc.x;
    }
	void Update()
	{

		if (rotating)
		{
			currentRotationOfRotatingDisc = new Vector3(
			Mathf.Lerp(currentRotationOfRotatingDisc.x, targetedRotationOfRotatingDisc.x, Time.deltaTime * rotateSpeed),
			Mathf.Lerp(currentRotationOfRotatingDisc.y, targetedRotationOfRotatingDisc.y, Time.deltaTime * rotateSpeed),
			Mathf.Lerp(currentRotationOfRotatingDisc.z, targetedRotationOfRotatingDisc.z, Time.deltaTime * rotateSpeed));

            currentRotationOfDigitsDisc = new Vector3(
            Mathf.LerpAngle(currentRotationOfDigitsDisc.x, targetedRotationOfDigitsDisc.x, Time.deltaTime * rotateSpeed),
            Mathf.LerpAngle(currentRotationOfDigitsDisc.y, targetedRotationOfDigitsDisc.y, Time.deltaTime * rotateSpeed),
            Mathf.LerpAngle(currentRotationOfDigitsDisc.z, targetedRotationOfDigitsDisc.z, Time.deltaTime * rotateSpeed));

            rotatingDisc.transform.eulerAngles = currentRotationOfRotatingDisc;
            digitsDisc.transform.eulerAngles = currentRotationOfDigitsDisc;
            if (Mathf.Abs(currentRotationOfRotatingDisc.x - targetedRotationOfRotatingDisc.x) <= 1)
			{
				rotatingDisc.transform.eulerAngles = targetedRotationOfRotatingDisc;
                digitsDisc.transform.eulerAngles = targetedRotationOfDigitsDisc;
                rotating = false;
			}
		}

	}
	private enum LockState
	{
		Zero,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
        Seven,
        Eight,
        Nine
    }

    void ChangeState(int input)
    {
        if (!locked || rotating)
            return;

        LockState var = (LockState)input;
        switch(var)
        {
            case LockState.Zero:
                userInput += '0';
                Rotate((int)var);
                break;
            case LockState.One:
                userInput += '1';
                Rotate((int)var);
                break;
            case LockState.Two:
                userInput += '2';
                Rotate((int)var);
                break;
            case LockState.Three:
                userInput += '3';
                Rotate((int)var);
                break;
            case LockState.Four:
                userInput += '4';
                Rotate((int)var);
                break;
            case LockState.Five:
                userInput += '5';
                Rotate((int)var);
                break;
            case LockState.Six:
                userInput += '6';
                Rotate((int)var);
                break;
            case LockState.Seven:
                userInput += '7';
                Rotate((int)var);
                break;
            case LockState.Eight:
                userInput += '8';
                Rotate((int)var);
                break;
            case LockState.Nine:
                userInput += '9';
                Rotate((int)var);
                break;
            default:
                break;
        }
        CompareCodes();
    }

    void CompareCodes()
    {
        if (userInput == code)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            locked = false;
        }
        for (int i = 0; i < userInput.Length; i++)
        {
            if (userInput[i] != code[i])
            {
                userInput = "";
                return;
            }
        }
    }

    void OpenOrClose()
    {
        if(locked == false)
        {
            if(opened == false)
            {
                GetComponent<HingeJoint>().useMotor = true;
                opened = true;
            }
            else
            {
                GetComponent<HingeJoint>().useMotor = false;
                opened = false;
            }
        }    
    }

    void Rotate(int input)
    {
        float correction = 0;
        if (turn == 1f)
        {
            if (input - lastRotation >= 0)
            {
                correction = 36 * (input - lastRotation);
            }
            else
            {
                correction = 360 - 36 * (lastRotation - input);
            }
        }
        else if (turn == -1f)
        {
            if (input - lastRotation < 0)
            {
                correction = 36 * (lastRotation - input);
            }
            else
            {
                correction = 360 - 36 * (input - lastRotation);
            }
            correction *= -1;
        }
        targetedRotationOfRotatingDisc = new Vector3(currentRotationOfRotatingDisc.x - correction, currentRotationOfRotatingDisc.y, currentRotationOfRotatingDisc.z);
        targetedRotationOfDigitsDisc = new Vector3(currentRotationOfDigitsDisc.x + correction, currentRotationOfDigitsDisc.y, currentRotationOfDigitsDisc.z);
        lastRotation = input;
        turn *= -1f;
        rotating = true;
    }
}
