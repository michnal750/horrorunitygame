﻿using UnityEngine;

public class Swoon : MonoBehaviour
{
    private bool isChanging;
    private bool isNight;
    public Light directionalLight;
    public float lightIntensityChange = 0.01f;
    public float targetLightIntensivity = 0.3f;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isNight && !isChanging)
        {
            isChanging = true;
        }
    }

    void Update()
    {
        if (isChanging)
        {
            directionalLight.intensity -= lightIntensityChange * Time.deltaTime;
            if (directionalLight.intensity <= targetLightIntensivity)
            {
                isChanging = false;
                isNight = true;
            }
        }
    }

    public bool[] getBools()
    {
        bool[] bools = { isChanging, isNight };
        return bools;
    }

    public void setBools(bool[] bools)
    {
        isChanging = bools[0];
        isNight = bools[1];
        if(isNight)
        {
            directionalLight.intensity = targetLightIntensivity;
        }
    }
}
