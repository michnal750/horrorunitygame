﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

	private void OnParticleCollision(GameObject other)
	{
		if(other.tag == "Deadly")
		{
			FindObjectOfType<GameManager>().EndGame();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Deadly")
		{
			FindObjectOfType<GameManager>().EndGame();
		}
	}

	public void SetPosition(Vector3 position)
	{
		FindObjectOfType<CharacterController>().enabled = false;
		gameObject.transform.position = position;
		FindObjectOfType<CharacterController>().enabled = true;
	}
}
