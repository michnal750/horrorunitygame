﻿using UnityEngine;

public class SecurityCameraOptimalization : MonoBehaviour
{
    //public GameObject securityCamera;
    public Camera camera;

    private void Start()
    {
        // securityCamera.SetActive(false);
        camera.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //securityCamera.SetActive(true);
            camera.enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //securityCamera.SetActive(false);
            camera.enabled = false;
        }
    }


}
