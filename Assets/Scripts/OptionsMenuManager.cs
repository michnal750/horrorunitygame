﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuManager : MonoBehaviour
{
    [SerializeField]
    GameObject MainMenu;
    [SerializeField]
    TMPro.TMP_Dropdown qualityDropdown;

    private void Start()
    {
        qualityDropdown.value = QualitySettings.GetQualityLevel();
    }
    public void ApplyAndGoBack()
    {
        QualitySettings.SetQualityLevel(qualityDropdown.value, true);
        this.gameObject.SetActive(false);
        MainMenu.SetActive(true);
    }
}
