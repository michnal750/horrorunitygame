﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class PlayerData: Data
	{
		public float[] position = new float[3];
		public PlayerData(PlayerManager player)
		{
			position[0] = player.transform.position.x;
			position[1] = player.transform.position.y;
			position[2] = player.transform.position.z;
		}
	}
}
