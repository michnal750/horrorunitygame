﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class DoorData : Data
	{
		public int[] doorList = new int[0];
		int counter = 0;
		public DoorData(DoorMechanism[] doors)
		{
			counter = doors.Length;
			int[] bools = new int[counter * 2];
			int i = 0;
			foreach(DoorMechanism dm in doors)
			{
				bools[i] = Convert.ToInt32(dm.opened);
				bools[i+1] = Convert.ToInt32(dm.locked);
				i += 2;
			}
			doorList = bools;
		}
	}
}
