﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class DestructableData: Data
	{
		public string[] destructableObjects = new string[0];
		int counter = 0;
		public DestructableData(GameObject[] gameObjects)
		{
			counter = gameObjects.Length;
			string[] obj = new string[counter];
			int i = 0;
			foreach(GameObject go in gameObjects)
			{
				obj[i] = go.name;
				i++;
			}
			destructableObjects = obj;
		}
	}
}
