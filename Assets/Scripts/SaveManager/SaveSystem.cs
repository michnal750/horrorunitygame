﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Assets.Scripts.SaveManager
{ 	
	public static class SaveSystem
	{
		public static void Save(Data data, string exactPath)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			string path = Application.persistentDataPath + exactPath;
			FileStream stream = new FileStream(path, FileMode.Create);			

			formatter.Serialize(stream, data);
			stream.Close();
		}

		public static Data Load(Data data, string exactPath)
		{
			string path = Application.persistentDataPath + exactPath;
			if (File.Exists(path))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				FileStream stream = new FileStream(path, FileMode.Open);

				data = formatter.Deserialize(stream) as Data;
				stream.Close();
				return data;
			}
			else return null;
		}
	}
}
