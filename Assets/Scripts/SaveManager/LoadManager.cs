﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadManager : MonoBehaviour
{
    bool reload;
    void Awake()
    {
        reload = Convert.ToBoolean(PlayerPrefs.GetInt("load"));
        LoadGame();
    }

    public void LoadGame()
    {
        if (reload)
        {
            FindObjectOfType<SaveManager>().load = true;
        }
        PlayerPrefs.SetInt("load", 0);
        reload = false;
    }

    public void TakeOrder()
    {
        PlayerPrefs.SetInt("load", 1);
    }
}
