﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class SwoonData : Data
	{
		public int isChanging;
		public int isNight;
		public float lightIntensity;
		public SwoonData(Swoon swoon)
		{
			bool[] bools = swoon.getBools();
			isChanging = Convert.ToInt32(bools[0]);
			isNight = Convert.ToInt32(bools[1]);
			lightIntensity = swoon.directionalLight.intensity;
		}
	}
}
