﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class DialogueData: Data
	{
		public int[] dialogues = new int[0];
		int counter = 0;
		public DialogueData(DialogueTrigger[] dialogueTriggers)
		{
			counter = dialogueTriggers.Length;
			int[] bools = new int[counter];
			int i = 0;
			foreach (DialogueTrigger dt in dialogueTriggers)
			{
				bools[i] = Convert.ToInt32(dt.GetWasTriggered());
				i++;
			}
			dialogues = bools;
		}
	}
}
