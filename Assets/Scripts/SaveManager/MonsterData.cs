﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class MonsterData: Data
	{
		public float[] position = new float[3];
		public MonsterData(MonsterBehaviour monster)
		{
			position[0] = monster.transform.position.x;
			position[1] = monster.transform.position.y;
			position[2] = monster.transform.position.z;
		}
	}
}
