﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class InventoryData : Data
	{
		public string[] itemList = new string[0];
		public string[] notesList = new string[0];
		public string[] collectableList = new string[0];
		public string[] noteObjectsList = new string[0];
		public InventoryData(Inventory inventory, string[] collectables, string[] notesL)
		{
			string[] items = new string[inventory.items.Count];
			for (int i = 0; i < Inventory.instance.items.Count; i++)
			{
				items[i] = Inventory.instance.items[i].name;
			}
			itemList = items;


			string[] notes = new string[inventory.notes.Count];
			for (int i = 0; i < Inventory.instance.notes.Count; i++)
			{
				notes[i] = Inventory.instance.notes[i].name;
			}
			notesList = notes;

			collectableList = collectables;
			noteObjectsList = notesL;
		}
	}
}
