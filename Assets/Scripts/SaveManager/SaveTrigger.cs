﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag=="Player")
        {
            Debug.Log("Saved");
            FindObjectOfType<SaveManager>().save = true;
        }
    }
}
