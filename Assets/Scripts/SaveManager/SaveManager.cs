﻿using Assets.Scripts.SaveManager;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	PlayerData playerData;
	InventoryData inventoryData;
	PickableObjectData pickableObjectData;
	MonsterData monsterData;
	DoorData doorData;
	DialogueData dialogueData;
	DestructableData destructableData;
	SafeData safeData;
	BlockadeData blockadeData;
	SwoonData swoonData;

	GameObject[] pickableObjects;
	DoorMechanism[] doors;
	DialogueTrigger[] dialogueTriggers;
	GameObject[] destructableObjects;

	bool saved = false;
	string playerPath = "/Player.txt";
	string inventoryPath = "/Inventory.txt";
	string monsterPath = "/Monster.txt";
	string pickableObjectsPath = "/PickableObjects.txt";
	string doorsPath = "/Doors.txt";
	string dialoguesPath = "/Dialogues.txt";
	string destructablePath = "/DestructableObjects.txt";
	string safePath = "/SafeObjects.txt";
	string blockadesPath = "/Blockades.txt";
	string swoonPath = "/Swoon.txt";
	public string[] notesPaths;
	public string[] itemsPaths;

	public bool load;
	public bool save;
	ObjectsTracker objectsTracker;
	void Start()
	{
		objectsTracker = new ObjectsTracker();
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Pickable Object");
		pickableObjects = objects;
		DoorMechanism[] doorObjects = FindObjectsOfType<DoorMechanism>();
		doors = doorObjects;
		dialogueTriggers = FindObjectsOfType<DialogueTrigger>();
	}

	void Update()
	{
		if (save)
			Save();
		if (load)
			Load();
	}

	void Save()
	{
		objectsTracker.Save();
		destructableObjects = objectsTracker.destructableObjects;
		SaveSystem.Save(new PlayerData(FindObjectOfType<PlayerManager>()), playerPath);
		SaveSystem.Save(new InventoryData(Inventory.instance, objectsTracker.collectableObjects, objectsTracker.noteObjects), inventoryPath);
		SaveSystem.Save(new PickableObjectData(pickableObjects), pickableObjectsPath);
		SaveSystem.Save(new MonsterData(FindObjectOfType<MonsterBehaviour>()), monsterPath);
		SaveSystem.Save(new DoorData(doors), doorsPath);
		SaveSystem.Save(new DialogueData(dialogueTriggers), dialoguesPath);
		SaveSystem.Save(new DestructableData(destructableObjects), destructablePath);
		SaveSystem.Save(new SafeData(objectsTracker.safes, objectsTracker.panels), safePath);
		SaveSystem.Save(new BlockadeData(objectsTracker.blockades), blockadesPath);
		SaveSystem.Save(new SwoonData(FindObjectOfType<Swoon>()), swoonPath);
		Debug.Log("Saved");
		save = false;
	}
	void Load()
	{
		objectsTracker.Load();
		playerData = SaveSystem.Load(playerData, playerPath) as PlayerData;
		FindObjectOfType<PlayerManager>().SetPosition(new Vector3(playerData.position[0], playerData.position[1], playerData.position[2]));
		monsterData = SaveSystem.Load(monsterData, monsterPath) as MonsterData;
		FindObjectOfType<MonsterBehaviour>().transform.position = (new Vector3(monsterData.position[0], monsterData.position[1], monsterData.position[2]));
		LoadInventory();
		pickableObjectData = SaveSystem.Load(pickableObjectData, pickableObjectsPath) as PickableObjectData;
		int j = 0;
		foreach (GameObject go in pickableObjects)
		{
			go.transform.position = new Vector3(pickableObjectData.position[j], pickableObjectData.position[j + 1], pickableObjectData.position[j + 2]);
			j += 3;
		}
		deleteCollectableObjectsFromScene();
		j = 0;
		doorData = SaveSystem.Load(doorData, doorsPath) as DoorData;
		foreach (DoorMechanism dm in doors)
		{
			dm.opened = Convert.ToBoolean(doorData.doorList[j]);
			dm.locked = Convert.ToBoolean(doorData.doorList[j + 1]);
			j += 2;
		}
		dialogueData = SaveSystem.Load(dialogueData, dialoguesPath) as DialogueData;
		j = 0;
		foreach (DialogueTrigger dt in dialogueTriggers)
		{
			dt.SetWasTriggered(Convert.ToBoolean(dialogueData.dialogues[j]));
		}
		destructableData = SaveSystem.Load(destructableData, destructablePath) as DestructableData;
		objectsTracker.Compare(destructableData.destructableObjects);

		safeData = SaveSystem.Load(safeData, safePath) as SafeData;
		SafeLock[] safes = FindObjectsOfType<SafeLock>();
		for (int i = 0; i < safes.Length; i++)
		{
			safes[i].locked = Convert.ToBoolean(safeData.safeLocked[i]);
			if (!safes[i].locked)
			{
				safes[i].SendMessage("OpenOrClose");
			}
		}
		DoorPanel[] panels = FindObjectsOfType<DoorPanel>();
		for (int i = 0; i < safeData.gateName.Length; i++)
		{

			DoorPanel temp = panels.Where(obj => obj.gameObject.name == safeData.gateName[i]).FirstOrDefault();
			if (temp != null)
			{
				string temp1 = safeData.gateCodes[i];
				for (int k = 0; k < safeData.gateCodes.Length; k++)
				{
					temp.userInput = temp1;
				}
				temp.SendMessage("OpenOrClose");
			}

		}

		blockadeData = SaveSystem.Load(blockadeData, blockadesPath) as BlockadeData;
		for (int i = 0; i < blockadeData.blockadeNames.Length; i++)
		{
			GameObject temp = objectsTracker.blockades.Where(obj => obj.gameObject.name == blockadeData.blockadeNames[i]).FirstOrDefault();
			if (temp != null)
			{
				if (blockadeData.ifBlastedOff[i] == 1)
				{
					temp.GetComponent<Highlight>().boulders.SetActive(temp.GetComponent<Highlight>().createObstacle);
					Destroy(temp);
				}
			}
		}
		swoonData = SaveSystem.Load(swoonData, swoonPath) as SwoonData;
		bool[] bools = { Convert.ToBoolean(swoonData.isChanging), Convert.ToBoolean(swoonData.isNight) };
		FindObjectOfType<Swoon>().setBools(bools);
		FindObjectOfType<Swoon>().directionalLight.intensity = swoonData.lightIntensity;
		load = false;
	}

	void deleteCollectableObjectsFromScene()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Collectable Object");
		GameObject[] objectsN = GameObject.FindGameObjectsWithTag("Note Object");

		foreach (GameObject go in objects)
		{
			if (go != null)
			{
				if (!inventoryData.collectableList.Contains(go.name))
					Destroy(go);
			}
		}

		foreach (GameObject go in objectsN)
		{
			if (go != null)
			{
				if (!inventoryData.noteObjectsList.Contains(go.name))
					Destroy(go);
			}
		}
	}

	void LoadInventory()
	{
		inventoryData = SaveSystem.Load(inventoryData, inventoryPath) as InventoryData;
		foreach (string s in inventoryData.itemList)
		{
			Item item = null;
			int i = 0;
			while (item == null)
			{
				item = (Item)Resources.Load(itemsPaths[i] + s, typeof(Item));
				i++;
				if (i > itemsPaths.Length)
				{
					break;
				}
			}

			_ = Inventory.instance.Add(item);
		}
		foreach (string s in inventoryData.notesList)
		{
			Note note = null;
			int i = 0;
			while (note == null)
			{
				note = (Note)Resources.Load(notesPaths[i] + s, typeof(Note));
				i++;
				if (i > notesPaths.Length)
				{
					break;
				}
			}
			_ = Inventory.instance.Add(note);
		}
	}
}
