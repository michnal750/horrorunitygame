﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.SaveManager
{
	public class ObjectsTracker : MonoBehaviour
	{
		GameObject[] destructableObjectsCompare;
		public GameObject[] destructableObjects;
		public string[] collectableObjects;
		public string[] noteObjects;
		public GameObject[] safes = new GameObject[0];
		public GameObject[] panels = new GameObject[0];
		public GameObject[] blockades = new GameObject[0];
		public void Load()
		{
			destructableObjectsCompare = GameObject.FindGameObjectsWithTag("DestructableObject");
		}
		public void Save()
		{
			destructableObjects = GameObject.FindGameObjectsWithTag("DestructableObject");
			GameObject[] tempGO = GameObject.FindGameObjectsWithTag("Collectable Object");
			GameObject[] tempNO = GameObject.FindGameObjectsWithTag("Note Object");
			collectableObjects = new string[tempGO.Length];
			for (int i = 0; i < tempGO.Length; i++)
			{
				collectableObjects[i] = tempGO[i].name;
			}

			noteObjects = new string[tempNO.Length];
			for (int i = 0; i < tempNO.Length; i++)
			{
				noteObjects[i] = tempNO[i].name;
			}

			List<GameObject> s = new List<GameObject>();
			List<GameObject> p = new List<GameObject>();

			SafeLock[] safesL = FindObjectsOfType<SafeLock>();
			DoorPanel[] panelsD = FindObjectsOfType<DoorPanel>();
			foreach(SafeLock sl in safesL)
			{
				s.Add(sl.gameObject);
			}
			foreach(DoorPanel dp in panelsD)
			{
				p.Add(dp.gameObject);
			}
			safes = s.ToArray();
			panels = p.ToArray();

			List<GameObject> blockadeL = new List<GameObject>();
			Highlight[] h = FindObjectsOfType<Highlight>();
			foreach(Highlight hl in h)
			{
				blockadeL.Add(hl.gameObject);
			}
			blockades = blockadeL.ToArray();
		}

		public void Compare(string[] loadedObjects)
		{
			List<string> destructable = new List<string>(loadedObjects);
			foreach (GameObject go in destructableObjectsCompare)
			{
				string result = destructable.Find(
				delegate (string r)
				{
					return r == go.name;
				}
				);
				if (result == null)
				{
					go.SendMessage("Substitute");
				}
			}
		}
	}
}
