﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class SafeData : Data
	{
		public int[] safeLocked = new int[0];
		public string[] gateCodes = new string[0];
		public string[] gateName = new string[0];
		public SafeData(GameObject[] safe, GameObject[] panel)
		{
			safeLocked = new int[safe.Length];
			gateName = new string[panel.Length];
			gateCodes = new string[panel.Length];
			int i;
			for (i = 0; i < safe.Length; i++)
			{
				SafeLock sl = safe[i].GetComponentInChildren<SafeLock>();
				if (sl != null)
				{
					safeLocked[i] = Convert.ToInt32(sl.locked);
				}
			}
			for (i = 0; i < panel.Length; i++)
			{
				DoorPanel dp = panel[i].GetComponentInChildren<DoorPanel>();
				if (dp != null)
				{
					gateCodes[i] = dp.userInput;
					gateName[i] = dp.gameObject.name;
				}
			}
		}
	}
}
