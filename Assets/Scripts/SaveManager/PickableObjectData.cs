﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class PickableObjectData: Data
	{
		public int counter = 0;
		public float[] position;

		public PickableObjectData(GameObject[] gameObjects)
		{
			counter = gameObjects.Length;
			position = new float[counter*3];
			int i = 0;
			foreach(GameObject go in gameObjects) 
			{
				position[i] = go.transform.position.x;
				position[i+1] = go.transform.position.y;
				position[i+2] = go.transform.position.z;
				i += 3;
			}
		}
	}
}
