﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Assets.Scripts.SaveManager
{
	[Serializable]
	public class BlockadeData: Data
	{
		public string[] blockadeNames = new string[0];
		public int[] ifBlastedOff = new int[0];
		public BlockadeData(GameObject[] blockades)
		{
			blockadeNames = new string[blockades.Length];
			ifBlastedOff = new int[blockades.Length];

			for(int i=0;i<blockades.Length;i++)
			{
				blockadeNames[i] = blockades[i].name;
				ifBlastedOff[i] = Convert.ToInt32(blockades[i].GetComponent<Highlight>().blastedOff);
			}
		}
	}
}
