﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Image icon;
    private Item item;
    public InspectorUI inspectorUI;
    private bool isEmpty = true;

    public void AddItem(Item item)
    {
        this.item = item;
        icon.sprite = item.icon;
        icon.enabled = true;
        isEmpty = false;
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        isEmpty = true;
    }

    public void SetInspector()
    {
        if (!isEmpty)
        {
            inspectorUI.LoadItemToInspector(item);
        }
        else
        {
            inspectorUI.ClearInspector();
        }
    }
}
