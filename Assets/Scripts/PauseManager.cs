﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public static bool gameIsPaused = false;
    public MouseLook mouseLook;
    public Flashlight flashlight;
    public PlayerMovement playerMovement;
    public GameObject hud;
    public GameObject inventoryPanel;
    public GameObject inventoryPage;
    public GameObject notePage;

    private void Start()
    {
        hud.SetActive(true);
        inventoryPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        gameIsPaused = false;
        mouseLook.enabled = true;
        flashlight.enabled = true;
        playerMovement.enabled = true;
        hud.SetActive(true);
        inventoryPanel.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void Pause()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        gameIsPaused = true;
        mouseLook.enabled = false;
        flashlight.enabled = false;
        playerMovement.enabled = false;
        hud.SetActive(false);
        inventoryPanel.SetActive(true);
        inventoryPage.SetActive(true);
        notePage.SetActive(false);
        Time.timeScale = 0.0f;
    }

    public void QuitGame()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu");
    }
}
