﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Highlight : MonoBehaviour
{
	public GameObject dynamites;
	public Item dynamite;
	public GameObject boulders;
	public GameObject[] childs;
	public bool createObstacle = false;

	public bool blastedOff = false;
	int size;
	int dynamitesPlaced = 0;
	float timer = 0;
	void Start()
	{
		BroadcastMessage("TurnOn");
		size = childs.Length;
		foreach (GameObject go in childs)
		{
			if (go != null)
				if (go.transform.tag == "Skeleton")
					go.SetActive(false);
		}
	}

	void Update()
	{
		if (dynamitesPlaced == size)
		{
			if (!blastedOff)
			{
				dynamites.BroadcastMessage("BlastOff");
				Destroy(gameObject, 4.8f);

			}
			else
			{
				timer += Time.deltaTime;
				if (timer >= 3.1f)
					boulders.SetActive(createObstacle);
			}
			blastedOff = true;
			
		}
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == "Player")
		{
			Inventory.instance.items.Contains(dynamite);
			foreach (GameObject go in childs)
			{
				if (go != null)
					if (go.transform.tag == "Skeleton")
					{
						go.SetActive(true);
					}
			}
			BroadcastMessage("TurnOn");
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.transform.tag == "Player")
		{
			Inventory.instance.items.Contains(dynamite);
			foreach (GameObject go in childs)
			{
				if (go != null)
					if (go.transform.tag == "Skeleton")
						go.SetActive(false);
			}

		}
	}

	void Add()
	{
		dynamitesPlaced++;
	}
}
