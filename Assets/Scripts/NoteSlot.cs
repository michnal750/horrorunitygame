﻿using UnityEngine;
using UnityEngine.UI;

public class NoteSlot : MonoBehaviour
{
    private Note note;
    public Image icon;
    private bool isEmpty = true;
    public GameObject noteInspectorObject;
    public NoteInspector noteInspector;

    public void AddItem(Note note)
    {
        this.note = note;
        icon.sprite = note.icon;
        icon.enabled = true;
        isEmpty = false;
    }

    public void UseNoteInspector()
    {
        if (!isEmpty)
        {
            noteInspectorObject.SetActive(true);
            noteInspector.SetNoteInspector(note);
        }
    }

}