﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerMechanism : MonoBehaviour
{
    private bool pickedUp = false;
    public float power;
    public Animator animator;
    private Rigidbody rigidbody;
    public GameObject holdingPoint;
    public GameObject hitPoint;
    void Update()
    {
        if (pickedUp == true)
        {
            transform.parent.parent = holdingPoint.transform;
            transform.parent.position = holdingPoint.transform.position;
            transform.parent.localRotation = Quaternion.Euler(0, 0, 0);
            transform.localPosition = new Vector3(-0.001070064f, -10.73461f, 2.551079e-05f);
            transform.localRotation = Quaternion.Euler(-0.08400001f, -144.969f, -0.059f);


            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                hitPoint.SetActive(false);
            }
            else if(animator.GetCurrentAnimatorStateInfo(0).IsName("Swing"))
            {
                hitPoint.SetActive(true);
            }
        }
    }

    void Take()
    {
        this.SendMessage("TurnOff");
        rigidbody = GetComponent<Rigidbody>();
        transform.parent.parent = holdingPoint.transform;
        transform.parent.position = holdingPoint.transform.position;
        transform.parent.localRotation = Quaternion.Euler(0, 0, 0);
        transform.localPosition = new Vector3(-0.001070064f, -10.73461f, 2.551079e-05f);
        transform.localRotation = Quaternion.Euler(-0.08400001f, -144.969f, -0.059f);

        animator.SetBool("isHolding", true);

        pickedUp = true;
    }
    void Drop()
    {
        pickedUp = false;
        animator.SetBool("isHolding", false);
        this.SendMessage("TurnOff");
        transform.parent.parent = null;
    }

    void Swing()
    {         
        animator.SetTrigger("swing");
        hitPoint.SetActive(true);
    }
}
