﻿using UnityEngine;

public class MonsterManager : MonoBehaviour
{
    private bool isFollowing;
    private float time;
    public Transform destinationPoint;
    public Transform player;
    public MonsterBehaviour monster;

    private int actualZone = 0;

    private void Update()
    {
        if (isFollowing)
        {
            time += Time.deltaTime;
            if (time >= 15.0f)
            {
                destinationPoint.position = player.position;
                Debug.Log("Aktualizuje punkt docelowy");
                time = 0.0f;
            }
        }
    }

    public void StartFollowPlayer(int zoneNumber)
    {
        if (actualZone != zoneNumber)
        {
            monster.NewZone(zoneNumber);
            actualZone = zoneNumber;
        }
        destinationPoint.position = player.position;
        isFollowing = true;
        Debug.Log("Zaczynam tropic gracza");
        monster.PlayerEnterZone();
    }

    public void StopFollowPlayer(int zoneNumber)
    {
        isFollowing = false;
        Debug.Log("Koncze tropic gracza");
        monster.PlayerExitZone();
    }
}
