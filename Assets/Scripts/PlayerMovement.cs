﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController characterController;
    public float movementSpeed = 8.0f;
    public float gravity = -9.81f;
    public float jumpHeight = 3.0f;
    Vector3 velocity;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;
    public AudioManager audioManager;
    public Slider slider;
    private bool isRunning;
    private float moveX;
    private float moveZ;
    private Vector3 move;

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -3.0f;
        }
        moveX = Input.GetAxis("Horizontal");
        moveZ = Input.GetAxis("Vertical");
        move = transform.right * moveX + transform.forward * moveZ;
        ManageRunning();
        characterController.Move(move * movementSpeed * Time.deltaTime);
        if (move.magnitude > 0.6f && isGrounded && !audioManager.IsPlaying("Walking"))
        {
            audioManager.Play("Walking");
        }
        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2.0f * gravity);
        }
        velocity.y += gravity * Time.deltaTime;
        characterController.Move(velocity * Time.deltaTime);
    }

    private void ManageRunning()
    {
        if (Input.GetButtonDown("Run") && moveZ > 0.5f && slider.value > 0.5f)
        {
            isRunning = true;
            movementSpeed *= 2.0f;
        }
        else if (Input.GetButtonUp("Run") && isRunning)
        {
            isRunning = false;
            movementSpeed /= 2.0f;
        }
        if (isRunning)
        {
            slider.value -= 0.1f * Time.deltaTime;
            if (slider.value <= 0.0f)
            {
                isRunning = false;
                movementSpeed /= 2.0f;
            }
        }
        else
        {
            slider.value += 0.1f * Time.deltaTime;
        }
    }
}
