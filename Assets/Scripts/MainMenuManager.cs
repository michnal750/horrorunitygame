﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject OptionsMenu;
    [SerializeField]
    private GameObject CreditsMenu;

    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("AMainScene");
    }

    public void ContinueGame()
    {
        PlayerPrefs.SetInt("load", 1);
        SceneManager.LoadScene("AMainScene");
    }
    public void OpenOptions()
    {
        this.gameObject.SetActive(false);
        OptionsMenu.SetActive(true);
    }
    public void OpenCredits()
    {
        this.gameObject.SetActive(false);
        CreditsMenu.SetActive(true);
    }


    public void QuitGame()
    {
        Application.Quit();
    }
}
