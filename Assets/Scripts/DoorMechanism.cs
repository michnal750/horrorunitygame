﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMechanism : MonoBehaviour
{
	AudioManager audioManager;
	public bool opened;
	public bool locked = true;
	public Item openingKey;

	void Start()
	{
		audioManager = FindObjectOfType<AudioManager>();
	}
	void ChangeState(List<Item> items)
	{
		if (openingKey != null)
		{
			if (locked == true && items.Contains(openingKey))
			{
				locked = false;
				audioManager.Play("DoorLock");
				return;
			}
			else if (locked == false)
			{
				OpenOrClose();
			}
			else
				audioManager.Play("DoorKnob");
		}
		else if (openingKey == null)
			OpenOrClose();
	}

	public void OpenOrClose()
	{
		if (locked == false)
		{
			audioManager.Play("OpenHeavyDoor");
			if (opened == false)
			{
				GetComponent<HingeJoint>().useMotor = false;
				opened = true;
			}
			else
			{
				GetComponent<HingeJoint>().useMotor = true;
				opened = false;
			}
		}
		else
			audioManager.Play("DoorKnob");
	}
}