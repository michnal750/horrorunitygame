﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabButton : MonoBehaviour
{

    float pressDistance = 0.03f;
    public bool pressed = true;
    public string direction;

    private void Start()
    {

    }
    private void Update()
    {
     
    }

    void Move()
    {
        pressDistance *= -1;
        if(direction == "x")
        transform.Translate(-pressDistance, 0, 0);
        else if(direction == "y")
        transform.Translate(0, -pressDistance, 0);
        else
        transform.Translate(0, 0, -pressDistance);
    }

    void PressButton()
    {
        if (!pressed)
        {
            Move();
            pressed = true;
        }
    }
}


