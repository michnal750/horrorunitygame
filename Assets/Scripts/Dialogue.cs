﻿using TMPro;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    private TextMeshProUGUI textField;
    private string[] dialogue;
    private int iterator;
    private float time;
    public float speedTime = 2.0f;
    private bool isDisplayed;

    void Start()
    {
        isDisplayed = false;
        textField = GetComponent<TextMeshProUGUI>();
        textField.text = "";
    }

    private void Update()
    {
        if (isDisplayed)
        {
            time += Time.deltaTime;
            if (time > speedTime)
            {
                time = 0.0f;
                ++iterator;
                if (iterator < dialogue.Length)
                {
                    textField.text = dialogue[iterator];
                }
                else
                {
                    textField.text = "";
                    isDisplayed = false;
                }
            }
        }
    }

    public void NewDialogue(string[] dialogue)
    {
        iterator = 0;
        this.dialogue = dialogue;
        textField.text = dialogue[0];
        isDisplayed = true;
    }
}
