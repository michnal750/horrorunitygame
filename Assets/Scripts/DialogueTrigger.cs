﻿using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public string[] dialogue;
    private bool wasTriggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!wasTriggered && other.tag == "Player")
        {
            FindObjectOfType<Dialogue>().NewDialogue(dialogue);
            wasTriggered = true;
        }
    }

    public bool GetWasTriggered()
    {
        return wasTriggered;
    }
    public void SetWasTriggered(bool b)
    {
        wasTriggered = b;
    }
}
