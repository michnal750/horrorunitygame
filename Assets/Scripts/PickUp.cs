﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public Camera camera;
    public float distance;
    public float smooth;
    private bool pickedUp = false;
    private Vector3 holdingRotation;
    public float power;
    private float xRotation;
    private float yRotation;
    private float zRotation;
    private Rigidbody rigidbody;
    // Update is called once per frame
    
    void Start()
    {
        camera = Camera.main;
    }
    void Update()
    {
        if (pickedUp)
        {
            this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, camera.transform.position + camera.transform.forward * distance, Time.deltaTime * smooth);

            xRotation = camera.transform.eulerAngles.x;
            yRotation = camera.transform.eulerAngles.y;
            zRotation = camera.transform.eulerAngles.z;
            transform.eulerAngles = holdingRotation + new Vector3(xRotation - camera.transform.eulerAngles.x, yRotation, zRotation);
        }
        else if (!pickedUp)
        {
            if (camera.enabled)
            {
                xRotation = camera.transform.eulerAngles.x;
                yRotation = camera.transform.eulerAngles.y;
                zRotation = camera.transform.eulerAngles.z;
            }
        }
    }

    void Take()
    {
        rigidbody = GetComponent<Rigidbody>();
        holdingRotation = this.transform.eulerAngles - new Vector3(xRotation - camera.transform.eulerAngles.x, yRotation, zRotation);
        pickedUp = true;
        rigidbody.useGravity = false;
    }
    void Drop()
    {
        pickedUp = false;
        this.SendMessage("TurnOff");
        rigidbody.isKinematic = false;
        rigidbody.useGravity = true;
    }

    void Throw()
    {
        pickedUp = false;
        rigidbody.isKinematic = true;
        Drop();
        rigidbody.AddForce(camera.transform.forward * power);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Collided with:"+ collision.gameObject.tag, collision.gameObject);
        this.SendMessage("TurnOff");
        if (pickedUp == true)
        {
            if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "MainCamera")
            {
                Physics.IgnoreCollision(collision.collider, this.GetComponent<Collider>());
            }
        }
    }

}
