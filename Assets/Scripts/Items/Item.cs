﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name;
    public string description;
    public Sprite icon;
    public GameObject gameObjectClone;
    public float examinedScale = 10.0f;
    public virtual void Use()
    {
        Debug.Log("Using " + name);
    }
}
