﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/FlashlightItem")]
public class FlashlightItem : Item
{
	public override void Use()
	{
		FindObjectOfType<Flashlight>().TurnOnOff();
	}
}
