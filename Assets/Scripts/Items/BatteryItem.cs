﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/BatteryItem")]
public class BatteryItem : Item
{
    public override void Use()
    {
        if (FindObjectOfType<Flashlight>().isInInventory)
        {
            FindObjectOfType<Flashlight>().insertNewBatteries();
            Inventory.instance.Remove(this);
        }
    }
}
