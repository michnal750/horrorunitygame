﻿using UnityEngine;
using UnityEngine.UI;

public class Flashlight : MonoBehaviour
{
    private Light spotLight;
    public Light pointLight;
    public AudioManager audioManager;
    public bool isDischarging = true;
    public float dischargingSpeed = 2.0f;
    public Image image;
    public Sprite[] sprite;
    private int chargeLevel;
    private readonly int maxChargeLevel = 100;
    private float time;
    public bool isInInventory = false;

    // Start is called before the first frame update
    void Start()
    {
        chargeLevel = maxChargeLevel;
        spotLight = GetComponent<Light>();
        RemoveFlashlight();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDischarging && spotLight.enabled)
        {
            time += Time.deltaTime;
            if (time >= dischargingSpeed)
            {
                --chargeLevel;
                changePicture();
                Debug.Log(chargeLevel);
                time = 0.0f;
            }
        }
        if (isInInventory && Input.GetButtonDown("Flashlight"))
        {
            TurnOnOff();
        }
    }

    public void TurnOnOff()
    {
        if (spotLight.enabled)
        {
            audioManager.Play("FlashlightOff");
            spotLight.enabled = false;
            pointLight.enabled = false;
            time = 0.0f;
        }
        else if (!spotLight.enabled && chargeLevel >= 1)
        {
            audioManager.Play("FlashlightOn");
            spotLight.enabled = true;
            pointLight.enabled = true;
        }
        else
        {
            audioManager.Play("FlashlightOn");
        }
    }

    public void EquipWithFlashlight()
    {
        isInInventory = true;
        image.enabled = true;
    }

    public void RemoveFlashlight()
    {
        isInInventory = false;
        spotLight.enabled = false;
        pointLight.enabled = false;
        image.enabled = false;
    }

    public void insertNewBatteries()
    {
        chargeLevel = maxChargeLevel;
        changePicture();
    }

    private void changePicture()
    {
        if (chargeLevel >= 66)
        {
            image.sprite = sprite[3];
        }
        else if (chargeLevel >= 33)
        {
            image.sprite = sprite[2];
            spotLight.intensity = 3.6f;
        }
        else if (chargeLevel >= 1)
        {
            image.sprite = sprite[1];
            spotLight.intensity = 3.3f;
        }
        else
        {
            image.sprite = sprite[0];
            spotLight.enabled = false;
            pointLight.enabled = false;
        }
    }
}
