﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swap : MonoBehaviour
{
	public Item dynamite;
	void SwapItems()
	{
		if(Inventory.instance.items.Contains(dynamite))
		{
			GameObject obj = Instantiate(dynamite.gameObjectClone, transform.position, transform.rotation);
			obj.transform.SetParent(transform.parent);
			obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			Inventory.instance.Remove(dynamite);
			obj.tag = "Untagged";
			gameObject.SendMessageUpwards("Add");
			Destroy(gameObject);
		}
	}
}
