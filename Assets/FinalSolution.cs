﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalSolution : MonoBehaviour
{
    public GameObject body;
    public GameObject camera;
    public GameObject Player;
    public DoorMechanism[] doors;
    public ParticleSystem[] particles;
    bool isDead = false;
    bool isOver = false;
    float time = 0.0f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (doors[0].opened)
                doors[0].OpenOrClose();
            if (doors[1].opened)
                doors[1].OpenOrClose();
            doors[0].tag = "Untagged";
            doors[1].tag = "Untagged";
           
            foreach (ParticleSystem p in particles)
            {
                p.Play();
            }
            isDead = true;
        }
    }

    private void Start()
    {
        foreach(ParticleSystem p in particles)
        {
            p.Pause();
        }
    }
    void Update()
    {
        
        if(isDead)
        {
            time += Time.deltaTime;
            if (time > 12.0f)
            {
                camera.SetActive(true);
                Player.SetActive(false);
                body.SetActive(true);
                isOver = true;
                time = 0.0f;
                isDead = false;
            }
        }
        if(isOver)
        {
           
            time += Time.deltaTime;
            if (time > 5.0f)
            {
                SceneManager.LoadScene("Menu");
            }
        }
    }
}
