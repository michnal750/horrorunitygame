﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenuManager : MonoBehaviour
{
    [SerializeField]
    GameObject MainMenu;
    public void GoBack()
    {
        this.gameObject.SetActive(false);
        MainMenu.SetActive(true);
    }
}
