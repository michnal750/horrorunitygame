# HorrorUnityGame

* Używamy Unity w wersji 2019.3.4 (najnowszej)
* Projekt uruchamiamy przez aplikacje Unity wybierajac odpowiedni folder
* Koniecznie nalezy sprawdzic czy VS jest prawidlowo podpiety do Unity.
Skrót klawiszowy Ctr + Shift + M powinien wyświetlić listę dostępnych funkcji.
Jeżeli nie wyświetla link poniżej:
https://stackoverflow.com/questions/42597501/autocompletion-not-working-in-visual-studio
* Konfiguracja narzędzia do mergowania:
https://gist.github.com/Ikalou/197c414d62f45a1193fd
* Wszystkie obiekty typu Item/Note mają miec pole Name takie same jak nazwa obiektu. 
Każdy przedmiot Pickable/Collectable/Item/Note ma mieć unikalną nazwę. Jeżeli tworzycie 
obikety, które się poruszają np bramy, każda powinna mieć unikalną nazwę.
* Foldery Items oraz Notes znajdują się w folderze Resources. Jeżeli dodany zostanie wewnątrz nich folder 
to na scenie w obiekcie Save Manager należy dodać ścieżki do tych folderów w skrypcie SaveManager
odpowiednio w notesPaths lub itemsPaths.